# get_block_domains

1. 2chアンテナサイトをスキップする公開スクリプトからアンテナサイトドメイン一覧取得
2. 取得したドメインへアクセスし、アンテナサイト内の2chまとめサイトドメイン一覧作成
3. 作成した一覧をdomain_list.txtに出力

## 使用方法

1. 必要に応じてwhite_domain.shにdomain_list.txtから除外するドメイン追記
2. ./get_domain_list.sh
3. 下記に収集したドメイン一覧が出力されるので適宜編集して使用
    1. domain_list.txt
