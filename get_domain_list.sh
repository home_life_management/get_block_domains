#!/bin/bash

set -euxo pipefail

cd $(dirname $0)
CURRENT_DIR="$(pwd)"

TMP_DIR="/tmp/get_domain_list"
HTML_DIR="${TMP_DIR%/}/html"
OUTPUT_FILE=${CURRENT_DIR%/}/domain_list.txt

# アンテナsite一覧をリンク先から取得し、アンテナsite内のドメインを収集する
#   https://greasyfork.org/ja/scripts/16465-skipantennasite/code

cleanup() {
  rm -rf $TMP_DIR
  mkdir -p $HTML_DIR
}

get_antenna_url_from_greasy_fork() {
  curl -fsL https://greasyfork.org/ja/scripts/16465-skipantennasite/code | \
    grep "@include" | \
    sed 's/.*:\/\/\(\*\.\)*\(.*\)\/.*/\2/'
}

get_antenna_html_files() {
  local antenna_domains_file="$1"
  local domain_num=1

  while IFS= read -r domain; do
    (curl -fsL $domain || echo "curl -fsL $domain failed") > ${HTML_DIR%/}/${domain_num}.html
    echo "Progress: $domain_num / $(wc -l $antenna_domains_file | cut -d' ' -f 1)"
    domain_num=$((domain_num+1))
  done < $antenna_domains_file
}

get_domain_from_html_file() {
  cd $HTML_DIR
  grep -hoE "https?://([a-zA-Z0-9\.\-])+" *.html | \
    sed -e 's/https*:\/\/\(www\.\?\)*//' | \
    sort | \
    uniq | \
    sed '/^.\{,3\}$/d' | \
    grep -vE "($(tr '\n' '|' < ${CURRENT_DIR%/}/white_domain.sh | rev | cut -c2- | rev))" | \
    grep -vE "(^[0-9\.]+$\.$)" | \
    sort
}

main() {
  cleanup
  get_antenna_url_from_greasy_fork > ${TMP_DIR%/}/antenna_domains.txt
  get_antenna_html_files ${TMP_DIR%/}/antenna_domains.txt
  get_domain_from_html_file > $OUTPUT_FILE

  echo "Domain list stored in $OUTPUT_FILE."
}

main
